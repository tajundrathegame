%BlockSize 32 32
%Texture N dragonN1.png
%Texture R dragonR1.png
%Texture V dragonV2.png

%Texture M pedrusco_cal1.png
%Texture N pedrusco_cal2.png
%Texture m pedrusco_cal3.png
%Texture n pedrusco_cal4.png

%Texture b pedrusco1.png
%Texture B pedrusco1M.png

%Texture t tierra1.png
%Texture r tierra2.png
%Texture p portal.png
%Texture P portal_abierto.png
%Texture Z piedra_runaz.png
%Texture z runaz.png

%CreateTajundraWithLife 240

%StageTitle Tutorial 4 - Large Shoots

%StageData 20 15
[                 p  ]
[             btrttrt]
[      Z  bb         ]
[    b bbbbbbb       ]
[    b b     bb      ]
[    b b     bbb     ]
[    b b     b bb    ]
[    b b     b  bb  |]
[    b b  $z b     bb]
[     M  btttb    bb ]
[    ttttt       bb  ]
[               bb   ]
[              bb    ]
[ |   @  B   |bb     ]
[trrttrtrtrtrtrt     ]


%NextStage stage_05_charge.st

%PlaceText 7 11
[<- Move the block to the left]
[and use jump and shoot combined]
[to destroy the weak block]

%PlaceText 1 11
[Use UP & SPACE to shoot]


