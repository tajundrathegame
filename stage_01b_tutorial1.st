%BlockSize 32 32
%Texture N dragonN1.png
%Texture R dragonR1.png
%Texture V dragonV2.png

%Texture M pedrusco_cal1.png
%Texture N pedrusco_cal2.png
%Texture m pedrusco_cal3.png
%Texture n pedrusco_cal4.png

%Texture b pedrusco1.png
%Texture B pedrusco1M.png

%Texture t tierra1.png
%Texture r tierra2.png
%Texture p portal.png
%Texture P portal_abierto.png
%Texture Z piedra_runaz.png
%Texture z runaz.png

%CreateTajundraWithLife 120

%StageTitle Tutorial 1b - The Basics II

%StageData 20 15
[bbMMMM  z    bbbbbbb]
[bbbbbbbbbbbb   bbbbb]
[bP       bbbbb   bbb]
[bbbbbbb   bbbbb   bb]
[bbbb    b   bbbb    ]
[bbb    trt     bb   ]
[bb    bbbbbbb     tr]
[     bbbbbbbbbbbbbbb]
[Z   b    bbbbbbbbbbb]
[rr         bbb  $  b]
[bbb    B     bbbbbbb]
[bbbbb||bbbbb    bbbb]
[bbbbbbbbbbbbbb    bb]
[b@             tttbb]
[bbbbbbbbbbbbbbbbbbbb]


%NextStage stage_02_tutorial2.st



