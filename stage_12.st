%BlockSize 32 32
%Texture N dragonN1.png
%Texture R dragonR1.png
%Texture V dragonV2.png

%Texture M pedrusco_cal1.png
%Texture N pedrusco_cal2.png
%Texture m pedrusco_cal3.png
%Texture n pedrusco_cal4.png

%Texture b pedrusco1.png
%Texture B pedrusco1M.png

%Texture t tierra1.png
%Texture r tierra2.png
%Texture p portal.png
%Texture P portal_abierto.png
%Texture Z piedra_runaz.png
%Texture z runaz.png

%CreateTajundraWithLife 240

%StageTitle Episode 1 - Level 3 - Spider

%StageData 20 15
[              b p   ]
[               trt  ]
[         B |      | ]
[         bbbb trtrtr]
[    $z|b    bZ      ]
[   trtrtr   bbbb    ]
[           bb    b  ]
[         b$zb     B ]
[        trtrt    trt]
[     B|             ]
[    bbbbb bbbbb     ]
[ @b  $   Z  $   b|  ]
[bbbbbbbbbbbbbbbbbbbb]
[                    ]
[                    ]


%NextStage stage_13.st


