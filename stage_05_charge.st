%BlockSize 32 32
%Texture N dragonN1.png
%Texture R dragonR1.png
%Texture V dragonV2.png

%Texture M pedrusco_cal1.png
%Texture N pedrusco_cal2.png
%Texture m pedrusco_cal3.png
%Texture n pedrusco_cal4.png

%Texture b pedrusco1.png
%Texture B pedrusco1M.png

%Texture t tierra1.png
%Texture r tierra2.png
%Texture p portal.png
%Texture P portal_abierto.png
%Texture Z piedra_runaz.png
%Texture z runaz.png

%CreateTajundraWithLife 240

%StageTitle Tutorial 5 - Charge Shoots

%StageData 20 15
[ @       z          ]
[bb         $        ]
[trrttrtrtrtrtrtr   Z]
[         z        bb]
[        |   $   bbb ]
[Z   rtrtrtrtrttrt   ]
[bb       z          ]
[ bbb $  |   $       ]
[   trtrtrtrtrrtr   Z]
[                  bb]
[      $   $ | $ bbb ]
[bb  ttrtrtrtrttrt   ]
[bbb                 ]
[ bbb $   $p         ]
[   trtrtrtrtrtrtrt  ]


%NextStage stage_10_gofast.st

%PlaceText 14 0
[Tajundra can make powerful shoots]
[Press SPACE for 4 seconds]
[and release it.]
[Try to not injure yourself!]


