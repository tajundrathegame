#!/usr/bin/python
#   encoding: UTF-8

import pygame
import random
import math
from stage import Stage
from qwesprites import *

pygame.init()

size = width, height = 640, 480
speed = [4,4]

screen = pygame.display.set_mode(size
            , pygame.DOUBLEBUF   # |  pygame.FULLSCREEN
                 )


def soft_update():
    nt=240
    lnt=range(0,nt)
    random.shuffle(lnt)
    for n in lnt:
        for l in range(0,480/nt):
            pygame.display.update((0,l*nt+n,640,1))
            pygame.time.wait(2)



img_splash = pygame.image.load("splash.jpg")
screen.blit(img_splash,(0,0))
soft_update()


#pygame.display.update()




stage_file="stage_00_game_menu.st"
######################################################
STAGE=Stage(stage_file)


frame=0

while frame<1000:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: Salir=True
        if event.type == pygame.KEYDOWN:
            frame+=10000

    pygame.time.wait(10)
    frame+=1

frame=0




######################################################

spriteTajundra = STAGE.Tajundra

######################################################


reloj_juego=pygame.time.Clock()
Salir=False
keys={}
time_o=pygame.time.get_ticks()

for ch in STAGE.simbolos:
    SpriteClass=None
    if ch=='@': SpriteClass=Tajundra
    if ch=='|': SpriteClass=LanzaBolas
    if ch=='$': SpriteClass=Dragon

    if SpriteClass:
        for n in range(0,len(STAGE.simbolos[ch])):
            SpriteClass(STAGE)

STAGE.draw(screen)
soft_update()

#STAGE.sprites+=[spriteTajundra,  t2,]
cpu_friendly=True
frame=0
accum_idle=0
cpu=0
benchmark=0
txtEndGame=None
fuerza=0
while not Salir:
    frame+=1
    if frame>60:
        frame=0
        cpu=100-accum_idle/10.0
        print len(STAGE.sprites), "%.2f%%" % cpu
        accum_idle=0
    if benchmark:
        if cpu<80:
            if frame > cpu-20 or frame==0 or frame==30:
                x,y,w,h=spriteTajundra.rect
                pos=x,y
                vel=300
                if spriteTajundra.dx<0: vel=-300
                vely=-120
                if keys.has_key(pygame.K_DOWN): vely=200
                if keys.has_key(pygame.K_UP): vely=-200
                Hacha(STAGE,pos,spriteTajundra.dx+vel,vely)
        elif frame==0:
            del STAGE.sprites[-1:]

    for event in pygame.event.get():
        if event.type == pygame.QUIT: Salir=True
        if event.type == pygame.KEYDOWN:
            if event.key==pygame.K_ESCAPE:
                Salir=True
            else:
                keys[event.key]=True

        if event.type == pygame.KEYUP:
            if keys.has_key(event.key):
                del keys[event.key]
            if spriteTajundra.alive:
                if event.key==pygame.K_SPACE:
                    sLife=spriteTajundra.Life
                    sMLife=spriteTajundra.MaxLife
                    if (fuerza>4000): fuerza=4000
                    fuerza*=sLife*sLife/(sMLife*sMLife)
                    x,y,w,h=spriteTajundra.rect
                    pos=x,y
                    vel=210+fuerza/100.0
                    vely=-vel/2
                    if keys.has_key(pygame.K_DOWN): vely=vel/2
                    if keys.has_key(pygame.K_UP): vely=-vel
                    if spriteTajundra.dx<0: vel=-vel
                    p=Hacha(STAGE,pos,spriteTajundra.dx+vel,vely)
                    p.Life+=fuerza/100.0-.5
                    fuerza=0

    if spriteTajundra.alive:
        if keys.has_key(pygame.K_s):
            spriteTajundra.alive=False
            spriteTajundra.LevelFinished=True

        if spriteTajundra.in_ground:

            if keys.has_key(pygame.K_LEFT):     spriteTajundra.dx-=15
            if keys.has_key(pygame.K_RIGHT):    spriteTajundra.dx+=15
            if keys.has_key(pygame.K_UP):       spriteTajundra.dy=-140
            if keys.has_key(pygame.K_DOWN):     spriteTajundra.dy+=5

        else:

            if spriteTajundra.dy<0:
                if not keys.has_key(pygame.K_UP):   spriteTajundra.dy/=1.02
                if keys.has_key(pygame.K_LEFT):     spriteTajundra.dx-=0.5
                if keys.has_key(pygame.K_RIGHT):    spriteTajundra.dx+=0.5
            else:
                if keys.has_key(pygame.K_UP):       spriteTajundra.dy/=1.02
                if keys.has_key(pygame.K_DOWN):     spriteTajundra.dy+=2

    else:
        if not txtEndGame:
            if spriteTajundra.LevelFinished:
                txtEndGame=qweTextBIG(STAGE,320,240,"Level Completed")
            else:
                txtEndGame=qweTextBIG(STAGE,320,240,"Game Over")
            qweTextBIG(STAGE,320,300,"press return")

        if keys.has_key(pygame.K_RETURN):
            if spriteTajundra.LevelFinished:
                stage_file=STAGE.NextStage

            for sprite in STAGE.sprites:
                del sprite

            del STAGE
            txtEndGame=None
            STAGE=Stage(stage_file)
            spriteTajundra = STAGE.Tajundra
            for ch in STAGE.simbolos:
                SpriteClass=None
                if ch=='@': SpriteClass=Tajundra
                if ch=='|': SpriteClass=LanzaBolas
                if ch=='$': SpriteClass=Dragon

                if SpriteClass:
                    for n in range(0,len(STAGE.simbolos[ch])):
                        SpriteClass(STAGE)
            STAGE.draw(screen)
            soft_update()

    # Render

    if cpu_friendly:
        time_aux=pygame.time.get_ticks()
        msec=time_aux-time_o;
        idle=1000/60-msec
        if idle<1: idle=1
        pygame.time.wait(idle)
        accum_idle+=pygame.time.get_ticks()-time_aux
        time_aux=pygame.time.get_ticks()
        msec=time_aux-time_o;
        time_o=time_aux
    else:
        msec=reloj_juego.tick(60)

    if keys.has_key(pygame.K_SPACE):    fuerza+=msec

    STAGE.fulldraw(screen,msec)

