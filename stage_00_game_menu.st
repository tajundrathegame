%BlockSize 32 32
%Texture N dragonN1.png
%Texture R dragonR1.png
%Texture V dragonV2.png

%Texture M pedrusco_cal1.png
%Texture N pedrusco_cal2.png
%Texture m pedrusco_cal3.png
%Texture n pedrusco_cal4.png

%Texture b pedrusco1.png
%Texture B pedrusco1.png

%Texture t tierra1.png
%Texture r tierra2.png
%Texture p portal.png
%Texture P portal_abierto.png
%Texture Z piedra_runaz.png
%Texture z runaz.png

%CreateTajundraWithLife 1000

%StageTitle Game Menu

%StageData 20 15
[                    ]
[                    ]
[                    ]
[                    ]
[                    ]
[                  $b]
[                  bb]
[                  b ]
[                  b ]
[                  z ]
[@                 Z|]
[bb                bb]
[bbb                 ]
[bbbb   bb          p]
[    tttttttttttttttt]

%PlaceText 8 5
[       - Tajundra the Game - ]
[      ]
[Press ESC at any time to exit]

%PlaceText 1 10
[<- You are here]

%PlaceText 3 11
[Use the arrows]
[to move Tajundra]

%PlaceText 7 11
[Use Up Arrow while]
[walking forward]
[to jump over the blocks]

%PlaceText 12 10
[Press SPACE to shoot]

%PlaceText 17 12
[>>>>]
[Enter the portal to]
[start a new game]

%NextStage stage_01_tutorial1.st

