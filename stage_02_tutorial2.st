%BlockSize 32 32
%Texture N dragonN1.png
%Texture R dragonR1.png
%Texture V dragonV2.png

%Texture M pedrusco_cal1.png
%Texture N pedrusco_cal2.png
%Texture m pedrusco_cal3.png
%Texture n pedrusco_cal4.png

%Texture b pedrusco1.png
%Texture B pedrusco1M.png

%Texture t tierra1.png
%Texture r tierra2.png
%Texture p portal.png
%Texture P portal_abierto.png
%Texture Z piedra_runaz.png
%Texture z runaz.png

%CreateTajundraWithLife 120

%StageTitle Tutorial 2 - Shooting

%StageData 20 15
[                    ]
[                    ]
[                    ]
[                    ]
[ @    M             ]
[      M             ]
[bbbbbbbbbbbbbbbbb   ]
[        M           ]
[  z     M   $       ]
[ bbb  trtrrtrtrtrtrt]
[bbbbb               ]
[rrtrtrtrtrtrt    Z  ]
[                bbb ]
[  p            bbbbb]
[rtrtrtrtrtrtrtrtrtrt]

%PlaceText 1 2
[Use space to throw axes.]
[Be very careful, your weapon ]
[bounces on walls and ceilings]
[So, you can injure yourself.]


%PlaceText 8 3
[These blocks are weak. You can]
[destroy them throwing axes several times]

%PlaceText 10 7
[This is your first enemy,]
[a dragon on strike.]
[Kill him to reach the rune]


%NextStage stage_03_movingblocks.st



