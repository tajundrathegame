%BlockSize 32 32
%Texture N dragonN1.png
%Texture R dragonR1.png
%Texture V dragonV2.png

%Texture M pedrusco_cal1.png
%Texture N pedrusco_cal2.png
%Texture m pedrusco_cal3.png
%Texture n pedrusco_cal4.png

%Texture b pedrusco1.png
%Texture B pedrusco1M.png

%Texture t tierra1.png
%Texture r tierra2.png
%Texture p portal.png
%Texture P portal_abierto.png
%Texture Z piedra_runaz.png
%Texture z runaz.png

%CreateTajundraWithLife 120

%StageTitle Tutorial 1 - The Basics

%StageData 20 15
[                    ]
[@     bb            ]
[ttrtrttrtrtttrtr    ]
[                    ]
[     B       ttrtrtt]
[  trtrtrr           ]
[          bbbb      ]
[tr     z            ]
[   bbtrttr          ]
[ bb      btr        ]
[           br    Z  ]
[               btrtb]
[              bbbbbb]
[ p           bbbbbbb]
[rtrtrtrtrtrtrtrtrtrt]

%PlaceText 1 0
[                ]
[Remember: use up arrow to jump]

%PlaceText 15 5
[Tajundra has limited life]
[If you don't complete the level]
[in 120 seconds, you'll die.]
[Eat some food to increase your life.]

%PlaceText 4 6
[The goal of the game is to collect]
[each rune of the level, and put those]
[on top of each rune stone.]

%PlaceText 13 8
[Now touch the stone.]

%PlaceText 0 12
[When all runes are in place, the]
[portal will open. Enter the portal]
[to complete the level]


%NextStage stage_01b_tutorial1.st



