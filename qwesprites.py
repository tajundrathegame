# Quick W Engine
#   encoding: UTF-8
import pygame
import math
import random


class qweSprite(pygame.sprite.Sprite):
    dx=dy=x=y=0
    w=h=32
    alive=True
    LevelFinished=False
    in_ground=False
    FRICTION_AIR=(0.2,0.2)
    FRICTION_FLOOR=(8,8)
    FRICTION_CEIL=(0.1,4)
    GRAVITY=5
    BOUNCE=-.1
    RADIUS=16
    psx=-1
    psy=-1
    ShowLife=MaxLife=Life=240
    DIE_IMG='pernil.png'
    accum_ms=1000
    accum_dist=1000

    REGEN=1
    #MAX_ACCUMMS=200
    #MAX_ACCUMDIST=1000

    MAX_ACCUMMS=100
    MAX_ACCUMDIST=500
    MAX_ITERMS=33

    #MAX_ACCUMMS=50
    #MAX_ACCUMDIST=250

    LifeCollisionCost=0
    LifeCollisionSpriteCost=2
    MARGIN_LEFT=+2
    MARGIN_RIGHT=-2
    MARGIN_TOP=+1
    MARGIN_BOTTOM=-1
    fcount=0
    SOLID=True
    FSOLID=True
    Layer=1
    Item="?"
    ItemTime=0

    def __init__(self, stage):
        pygame.sprite.Sprite.__init__(self)
        if self.Layer==1: stage.sprites+=[self]
        if self.Layer==2: stage.background_sprites+=[self]

    def move(self,x,y):
        self.x+=x
        self.y+=y
        d=math.sqrt(x*x+y*y)
        self.accum_dist+=d*1000.0

        self.rect=pygame.Rect(self.x,self.y,self.w,self.h)

    def selectImage(self):
        return self.image

    def die(self,screen):
        Life(screen,(int(self.x),int(self.y)),self.DIE_IMG,self.MaxLife/10.0)


    def draw(self,screen,list_rects=None):
        if self.accum_ms>0:
            if list_rects:
                if self.rect.collidelist(list_rects)==.1: return
        self.fcount+=1
        if self.fcount>10/(self.Life+1):
            self.fcount=4
            screen.blit(self.selectImage(), self.rect)
            if self.ShowLife>self.Life or (self.Life < 60 and self.ShowLife>0):
                if self.ShowLife<0: self.ShowLife=0
                x,y,h,w=self.rect
                pp=100.0*self.ShowLife/self.MaxLife
                pp=pp+math.log(1+1.9*self.ShowLife/self.MaxLife)*100.0
                if pp>200.0:
                    pp=200
                if pp>100.0:
                    pp1=100
                    pp2=pp-100
                else:
                    pp1=pp
                    pp2=0

                w-=8
                x+=1
                y+=0
                p1=x,y
                p2=x+pp1*w/100.0,y
                p1a=x,y+1
                p2a=x+pp2*w/100.0,y+1

                p=255.0*pp1/100.0
                if pp2:
                    p=255.0*pp2/100.0
                    pygame.draw.line(screen, (255-p,255,0) , p1a, p2a,1)
                    pygame.draw.line(screen, (255-p,255,0) , p1, p2,1)
                else:
                    pygame.draw.line(screen, (255,p,0) , p1, p2,1)

    def tickms(self, ms, stage):
        if not self.alive:
            self.MAX_ACCUMMS = -1


        rect1=pygame.Rect(self.rect)
        d=math.sqrt(self.dx*self.dx+self.dy*self.dy)
        self.accum_ms+=ms
        #self.accum_dist+=d*self.accum_ms
        if self.ShowLife>self.Life:
            self.accum_dist+=(self.ShowLife-self.Life)

        if self.Life<1:
            self.accum_dist+=10/(self.Life+0.1)

        if self.accum_ms>self.MAX_ACCUMMS or \
            self.accum_dist+d*self.accum_ms>self.MAX_ACCUMDIST:
            ms=self.accum_ms
            self.accum_ms=0
            self.accum_dist=0

            if (self.ItemTime>0):
                self.ItemTime-=ms
            else:
                self.ItemTime=0

            if (self.Life<0):
                self.alive=False
                self.die(stage)
            else:
                if (self.REGEN<0 or self.Life<self.MaxLife):
                    self.Life+=self.REGEN*ms/1000.0

            if self.ShowLife>self.Life:
                self.ShowLife-=ms/100.0
                if self.ShowLife<self.Life:
                    self.ShowLife=self.Life
            factor=1
            if d*self.accum_ms>1000: factor=d*self.accum_ms/1000.0
            while(ms>0):
                iter_ms=ms
                if (iter_ms>self.MAX_ITERMS/factor): iter_ms=self.MAX_ITERMS/factor

                self.checkpos(stage,iter_ms)
                self.move(self.dx*iter_ms/1000.0,self.dy*iter_ms/1000.0)
                self.dx/=1+self.FRICTION_AIR[0]*iter_ms/1000.0
                self.dy/=1+self.FRICTION_AIR[1]*iter_ms/1000.0
                ms-=iter_ms

            rect2=self.rect
            self.psx,self.psy=self.getpspoint(stage,0,0);

            return [rect1.union(rect2)]
        else:
            return []


    def getpspoint(self,stage,px,py):
        sx,sy=stage.BlockSize
        x,y,w,h=self.rect

        if px>0:    x0=x+px
        elif px<0:  x0=x+w+px
        else:       x0=x+w/2

        if py>0:    y0=y+py
        elif py<0:  y0=y+h+py
        else:       y0=y+h/2

        x0=int(x0)/sx
        y0=int(y0)/sy

        return (x0,y0)

    def collision(self,stage,x,y,char):
        if self.ItemTime==0:
            Pantalla=stage.StageData

            if char=='P':
                self.alive=False
                self.LevelFinished=True

            if not self.Item:
                if char=='z':
                    self.Item=char
                    self.ItemTime=1000
                    stage.setCh(x,y,' ')
                    stage.checkPortal()
            else:
                if char=='Z' and self.Item=='z' and stage.getCh(x,y-1)==' ':
                    testrect=pygame.Rect(x*32+8,(y-1)*32+8,16,16)

                    put_in_place=True
                    for sprite in stage.sprites:
                        if sprite.FSOLID and sprite.rect.colliderect(testrect):
                            put_in_place=False

                    if put_in_place:
                        stage.setCh(x,y-1,'z')
                        stage.checkPortal()
                        self.ItemTime=1000
                        self.Item=''


    def checkcell(self,stage,px,py):
        Pantalla=stage.StageData

        x0,y0=self.getpspoint(stage,px,py);

        if y0<0 and x0>=0 and x0<len(Pantalla[0]): return ' '
        if y0>=0 and y0<len(Pantalla):
            if x0>=0 and x0<len(Pantalla[y0]):
                ch=Pantalla[y0][x0]
                self.collision(stage,x0,y0,ch)

                return ch

        return 't'

    def checkpos(self,stage,ms):
        self.in_ground=False

        if self.Life<1:
            MAX_ACCUMMS=1
            self.REGEN=-1
        if self.Life>0.5:
            for sprite in stage.sprites:
                if sprite!=self and sprite.RADIUS>0 and sprite.Life>0.5:
                    if ( math.fabs(sprite.psx-self.psx)<=1
                        and math.fabs(sprite.psy-self.psy)<=1):
                        x,y,w,h=self.rect
                        sx,sy,sw,sh=sprite.rect

                        min_d=self.RADIUS+sprite.RADIUS

                        ax=x-sx;ay=y-sy;
                        d1=math.sqrt(ax*ax+ay*ay)
                        if d1<min_d:
                            x+=self.dx/1000.0
                            y+=self.dy/1000.0
                            sx+=sprite.dx/1000.0
                            sy+=sprite.dy/1000.0

                            ax=x-sx;ay=y-sy;
                            d2=math.sqrt(ax*ax+ay*ay)
                            if d2<=d1:
                                sdx=sprite.dx
                                sdy=sprite.dy
                                if sprite.SOLID and self.FSOLID:
                                    sprite.dx=self.dx
                                    sprite.dy=self.dy

                                if self.SOLID and sprite.FSOLID:
                                    self.dx=sdx
                                    self.dy=sdy

                                if self.Life<900:
                                    self.Life-=sprite.LifeCollisionSpriteCost
                                    if self.Life<0.5:   self.Life=0.5
                                    if self.ShowLife>0:
                                        self.ShowLife=self.Life+10

                                if sprite.Life<900:
                                    sprite.Life-=self.LifeCollisionSpriteCost
                                    if sprite.Life<0.5:
                                        sprite.Life=0.5
                                    if sprite.ShowLife>0:
                                        sprite.ShowLife=sprite.Life+10


                            if d2<=d1+0.1:
                                self.in_ground=True
                                x,y,w,h=self.rect
                                ax=x-sx;ay=y-sy;
                                d1=math.sqrt(ax*ax+ay*ay)+1
                                if self.SOLID and sprite.FSOLID:
                                    self.dx+=20*ax/d1
                                    self.dy+=20*ay/d1

                                if sprite.SOLID and self.FSOLID:
                                    sprite.dx-=20*ax/d1
                                    sprite.dy-=20*ay/d1



        # Colisión Horizontal
        if ( (self.checkcell(stage,self.MARGIN_RIGHT,0)!=' ' and self.dx>=0) or
             (self.checkcell(stage,self.MARGIN_RIGHT,12)!=' ' and self.dx>=0) or
             (self.checkcell(stage,self.MARGIN_RIGHT,-12)!=' ' and self.dx>=0) ) :
            if self.Life<900: self.Life-=self.LifeCollisionCost
            self.move(-self.dx*ms/1000.0-.1,0)
            if (self.BOUNCE>0): self.dx*=-self.BOUNCE
            else: self.dx/=1-self.BOUNCE

        if ( (self.checkcell(stage,self.MARGIN_LEFT,0)!=' ' and self.dx<=0) or
             (self.checkcell(stage,self.MARGIN_LEFT,12)!=' ' and self.dx<=0) or
             (self.checkcell(stage,self.MARGIN_LEFT,-12)!=' ' and self.dx<=0) ) :
            if self.Life<900: self.Life-=self.LifeCollisionCost
            self.move(-self.dx*ms/1000.0+.1,0)
            if (self.BOUNCE>0): self.dx*=-self.BOUNCE
            else: self.dx/=1-self.BOUNCE

        if ( (self.checkcell(stage,self.MARGIN_RIGHT-3,0)!=' ') and
             (self.checkcell(stage,self.MARGIN_RIGHT-3,12)!=' ') and
             (self.checkcell(stage,self.MARGIN_RIGHT-3,-12)!=' ') ) :
            self.move(-3,0)

        if ( (self.checkcell(stage,self.MARGIN_LEFT+3,0)!=' ') and
             (self.checkcell(stage,self.MARGIN_LEFT+3,12)!=' ') and
             (self.checkcell(stage,self.MARGIN_LEFT+3,-12)!=' ') ) :
            self.move(+3,0)


        # Control de hundimiento
        if self.dy>=0:
            if (self.checkcell(stage,self.MARGIN_LEFT+4,self.MARGIN_BOTTOM-1)!=' '
                or self.checkcell(stage,self.MARGIN_RIGHT-4,self.MARGIN_BOTTOM-1)!=' ') and \
                (self.checkcell(stage,self.MARGIN_LEFT+4,self.MARGIN_TOP+1)==' '
                and self.checkcell(stage,self.MARGIN_RIGHT-4,self.MARGIN_TOP+1)==' '):
                self.move(0,-1)
                self.dy=0

        if (self.checkcell(stage,0,0)!=' '):
            self.move(0,-1)
            self.dy=0
        # Colisión de techo
        elif self.dy<0:
            if (self.checkcell(stage,self.MARGIN_LEFT+4,self.MARGIN_TOP)!=' ' or
                self.checkcell(stage,self.MARGIN_RIGHT-4,self.MARGIN_TOP)!=' ' or
                self.checkcell(stage,0,self.MARGIN_TOP)!=' '):

                if self.Life<900: self.Life-=self.LifeCollisionCost
                self.move(0,-self.dy*ms/1000.0)
                if (self.BOUNCE>0): self.dy*=-self.BOUNCE
                else: self.dy/=1-self.BOUNCE
                self.dx/=1+self.FRICTION_CEIL[0]*ms/1000.0
                self.dy/=1+self.FRICTION_CEIL[1]*ms/1000.0
                if self.checkcell(stage,0,+2)!=' ' and self.dy<0:
                    self.dx/=1+self.FRICTION_CEIL[0]*ms/1000.0
                    self.dy/=1+self.FRICTION_CEIL[1]*ms/1000.0
                    if self.checkcell(stage,0,+3)!=' ' and self.dy<0:
                        self.dy=0



        # Colisión suelo
        if ((self.checkcell(stage,self.MARGIN_LEFT+4,self.MARGIN_BOTTOM)!=' '
            and self.checkcell(stage,0,self.MARGIN_BOTTOM)!=' ')
            or (self.checkcell(stage,self.MARGIN_RIGHT-4,self.MARGIN_BOTTOM)!=' '
            and self.checkcell(stage,0,self.MARGIN_BOTTOM)!=' ')
                ):

            self.dx/=1+self.FRICTION_FLOOR[0]*ms/1000.0
            if self.dy>0:
                if self.Life<900: self.Life-=self.LifeCollisionCost
                if (self.BOUNCE>0): self.dy*=-self.BOUNCE
                else: self.dy/=1-self.BOUNCE
                self.dy/=1+self.FRICTION_FLOOR[1]*ms/1000.0
                self.move(0,-self.dy*ms/1000.0)

            elif self.dy<0:
                pass

            self.in_ground=True
        else:
        # Gravedad
            if self.checkcell(stage,self.MARGIN_LEFT+4,self.MARGIN_BOTTOM)!=' ': self.dx+=1
            if self.checkcell(stage,self.MARGIN_RIGHT-4,self.MARGIN_BOTTOM)!=' ': self.dx-=1
            self.dy+=self.GRAVITY

        if self.Life>self.MaxLife*1.5:   self.Life=self.MaxLife*1.5

class qweText(qweSprite):
    FONT=None
    GRAVITY=0
    SOLID=False
    FSOLID=False
    Life=1000
    LifeCollisionSpriteCost=0
    ShowLife=0
    RADIUS=0
    MAX_ACCUMMS=0
    MAX_ACCUMDIST=0

    def __init__(self, stage,x,y,txt,font,color):

        qweSprite.__init__(self,stage)
        self.FONT=font

        self.image=font.render(txt, True, color)

        self.rect = self.image.get_rect()

        self.x,self.y,self.w,self.h=self.rect
        self.x=x
        self.y=y

    def collision(self,stage,x,y,char):
        return

    def checkcell(self,stage,px,py):
        return ' '

    def checkpos(self,stage,ms):
        return


global qweTextBasic_font
qweTextBasic_font=None
global qweTextBIG_font
qweTextBIG_font=None

class qweTextBasic(qweText):
    def __init__(self, stage,x,y,txt):
        global qweTextBasic_font
        if not qweTextBasic_font:
            qweTextBasic_font=pygame.font.Font(None,15)

        color=(0,255,0)
        self.Layer=2
        qweText.__init__(self,stage,x,y,txt,qweTextBasic_font,color)

class qweTextBIG(qweText):
    def __init__(self, stage,x,y,txt):
        qweSprite.__init__(self,stage)
        global qweTextBIG_font
        if not qweTextBIG_font:
            qweTextBIG_font=pygame.font.Font(None,64)

        self.FONT=qweTextBIG_font

        self.image1=qweTextBIG_font.render(txt, True, (0,0,0))
        self.image2=qweTextBIG_font.render(txt, True, (255,255,255))
        w,h = self.image1.get_size()
        w+=5
        h+=5
        self.image=pygame.Surface((w,h),pygame.SRCALPHA).convert_alpha()
        self.image.fill((0,0,0,0))
        self.image.blit(self.image1,(5,5))
        self.image.blit(self.image2,(0,0))


        self.rect = self.image.get_rect()

        self.x,self.y,self.w,self.h=self.rect
        self.x=x-w/2
        self.y=y-h/2


class Tajundra(qweSprite):
    image_flip=None
    estado=0
    estado_ms=0
    MARGIN_LEFT=+8
    MARGIN_RIGHT=-8
    MARGIN_TOP=+1
    MARGIN_BOTTOM=-1
    RADIUS=10
    BOUNCE=0
    Item=None
    DIE_IMG='platano.png'
    MAX_ACCUMMS=50
    MAX_ACCUMDIST=250
    REGEN=-1

    def __init__(self, stage):

        qweSprite.__init__(self,stage)
        self.image_right = pygame.image.load('tajuA1.png')
        self.image_left=pygame.transform.flip(self.image_right,True,False)

        self.imageb_right = pygame.image.load('tajuA2.png')
        self.imageb_left=pygame.transform.flip(self.imageb_right,True,False)


        self.image=self.image_right
        self.rect = self.image.get_rect()
        pos=stage.simbolos['@'][0]
        del stage.simbolos['@'][0]
        self.rect.bottomright = pos
        self.rect=self.rect.move(stage.BlockSize)
        self.x,self.y,self.w,self.h=self.rect

    def selectImage(self):
        dx=self.dx
        if self.estado==0:
            if dx<0:
                self.image=self.image_left
            else:
                self.image=self.image_right
        else:
            if dx<0:
                self.image=self.imageb_left
            else:
                self.image=self.imageb_right
        self.estado_ms+=1
        if self.estado_ms>20:
            self.estado_ms=0
            if self.estado==0:
                self.estado=1
            else:
                self.estado=0
        return self.image





class Dragon(qweSprite):
    image_flip=None
    estado=0
    estado_ms=0
    MAX_ACCUMMS=200
    MAX_ACCUMDIST=1000
    MAX_ITERMS=100
    BOUNCE=-.1
    direction=1

    def __init__(self, stage):

        qweSprite.__init__(self,stage)
        self.image_left = pygame.image.load('dragonN1.png')
        self.image_right=pygame.transform.flip(self.image_left,True,False)

        self.imageb_left = pygame.image.load('dragonN2.png')
        self.imageb_right=pygame.transform.flip(self.imageb_left,True,False)


        self.image=self.image_right
        self.rect = self.image.get_rect()
        pos=stage.simbolos['$'][0]
        del stage.simbolos['$'][0]

        self.rect.bottomright = pos
        self.rect=self.rect.move(stage.BlockSize)
        self.x,self.y,self.w,self.h=self.rect

    def selectImage(self):
        dx=self.dx
        if self.estado==0:
            if dx<0:
                self.image=self.image_left
            else:
                self.image=self.image_right
        else:
            if dx<0:
                self.image=self.imageb_left
            else:
                self.image=self.imageb_right
        self.estado_ms+=1
        if self.estado_ms>20:
            self.estado_ms=0
            if self.estado==0:
                self.estado=1
            else:
                self.estado=0
        return self.image

    def tickms(self, ms, stage):
        if self.dx<-10: self.direction=-1
        if self.dx>+10: self.direction=+1

        if self.checkcell(stage,self.MARGIN_RIGHT,0)!=' ': self.direction=-1
        if self.checkcell(stage,self.MARGIN_LEFT,0)!=' ': self.direction=+1

        if self.checkcell(stage,self.MARGIN_RIGHT,self.MARGIN_BOTTOM)==' ': self.direction=-1
        if self.checkcell(stage,self.MARGIN_LEFT,self.MARGIN_BOTTOM)==' ': self.direction=+1
        v=qweSprite.tickms(self, ms, stage)
        self.dx+=self.direction

        return v

class LanzaBolas(qweSprite):
    image_flip=None
    MAX_ACCUMMS=200
    MAX_ACCUMDIST=1000
    MAX_ITERMS=200
    BOUNCE=.2

    estado=0
    estado_ms=0
    BOUNCE=0
    DIE_IMG='safanoria.png'
    def __init__(self, stage):

        qweSprite.__init__(self,stage)
        self.image = pygame.image.load('lanzabolas1.png')

        self.rect = self.image.get_rect()
        pos=stage.simbolos['|'][0]
        del stage.simbolos['|'][0]

        self.rect.bottomright = pos
        self.rect=self.rect.move(stage.BlockSize)
        self.x,self.y,self.w,self.h=self.rect
        self.estado_ms=random.randint(6000,12000)

    def selectImage(self):
        return self.image

    def tickms(self, ms, stage):

        v=qweSprite.tickms(self, ms, stage)
        self.estado_ms+=ms
        if self.estado_ms>10000 and self.Life>10:
            random.seed()
            self.estado_ms=random.randint(0,1000)
            r=random.randint(-1000,1000)/3.0
            disparo=Bola(stage,(int(self.x),int(self.y)),r,-200)
            disparo.move(0,-10)
            disparo.GRAVITY=1

        return v



class qweShoot(qweSprite):
    estado=0
    estado_ms=0
    FRICTION_AIR=(0.0,0.0)
    FRICTION_FLOOR=(0.0,0.0)
    FRICTION_CEIL=(0.0,0.0)
    GRAVITY=0
    BOUNCE=.5
    MaxLife=Life=2.0
    ShowLife=0
    LifeCollisionCost=1
    LifeCollisionSpriteCost=10
    REGEN=-1

    fcount=0
    accum_ms=0

    MARGIN_LEFT=+10
    MARGIN_RIGHT=-10
    MARGIN_TOP=+10
    MARGIN_BOTTOM=-10

    def __init__(self, stage, pos,dx,dy):

        qweSprite.__init__(self,stage)
        self.image = pygame.image.load('bola1.png')

        self.rect = self.image.get_rect()

        self.rect.topleft = pos
        self.dx=dx
        self.dy=dy
        self.x,self.y,self.w,self.h=self.rect
        d=math.sqrt(dx*dx+dy*dy)+5
        self.tickms(1000/d,stage);

    def collision(self,stage,x,y,char):
        qweSprite.collision(self,stage,x,y,char)
        if self.ItemTime==0:
            nchar=char

            if char=='M': nchar='N'
            if char=='N': nchar='m'
            if char=='m': nchar='n'
            if char=='n': nchar=' '

            if char!=nchar:
                stage.setCh(x,y,nchar)
                self.Life-=1
                self.ItemTime=1000



    def die(self,screen):
        pass

class Bola(qweShoot):
    FRICTION_AIR=(1.0,1.0)
    GRAVITY=20
    BOUNCE=.5
    MaxLife=Life=12.0
    LifeCollisionCost=1
    RADIUS=5
    MAX_ACCUMMS=200
    MAX_ACCUMDIST=1000
    MAX_ITERMS=200

class Hacha(qweShoot):
    FRICTION_AIR=(0.5,0.0)
    BOUNCE=.8
    MaxLife=60
    Life=2
    LifeCollisionCost=0.1
    LifeCollisionSpriteCost=10
    fcountrot=0
    angle=0
    GRAVITY=5
    RADIUS=10

    MARGIN_LEFT=+12
    MARGIN_RIGHT=-12
    MARGIN_TOP=+7
    MARGIN_BOTTOM=-7

    time_elapsed=0

    def __init__(self, stage, pos,dx,dy):

        qweSprite.__init__(self,stage)
        self.image1 = pygame.image.load('hacha1.png')
        if dx<0: self.image1=pygame.transform.flip(self.image1, True, False)
        self.image = self.image1.copy()
        self.rect = self.image.get_rect()

        self.rect.topleft = pos
        self.dx=dx
        self.dy=dy
        self.x,self.y,self.w,self.h=self.rect


    def draw(self,screen,lr=None):
        self.fcountrot+=1
        if self.Life>0 and self.fcountrot>5/self.Life:
            self.image=pygame.transform.rotate(self.image1, self.angle)
            if self.dx>0:
                self.angle-=90
            else:
                self.angle+=90
            self.fcountrot=0
        if self.Life>2 or self.Life<.5:
            self.SOLID=False
        else:
            self.SOLID=False

        qweShoot.draw(self,screen,lr)

    def tickms(self, ms, stage):
        if self.Life>2:
            self.dx*=1.01
            self.dy*=1.01
        self.time_elapsed+=ms
        if self.time_elapsed<50:
            self.FSOLID=False
        else:
            self.FSOLID=True

        v=qweSprite.tickms(self, ms, stage)
        return v

    def collision(self,stage,x,y,char):
        if self.ItemTime>0: return
        qweShoot.collision(self,stage,x,y,char)

        if char=='B' and x>self.psx and self.dx>0 \
            and stage.getCh(x+1,y)==' ': # and stage.getCh(x+1,y+1)!=' ':
            self.Life-=1
            self.ItemTime=1000

            testrect=pygame.Rect((x+1)*32,(y)*32,32,32)
            put_in_place=True
            for sprite in stage.sprites:
                if sprite.FSOLID and sprite.rect.colliderect(testrect):
                    put_in_place=False

            if put_in_place:
                stage.setCh(x,y,' ')
                stage.setCh(x+1,y,'B')

        if char=='B' and x<self.psx and self.dx<0 \
            and stage.getCh(x-1,y)==' ':  # and stage.getCh(x-1,y+1)!=' ':
            self.Life-=1
            self.ItemTime=1000

            testrect=pygame.Rect((x-1)*32,(y)*32,32,32)
            put_in_place=True
            for sprite in stage.sprites:
                if sprite.FSOLID and sprite.rect.colliderect(testrect):
                    put_in_place=False

            if put_in_place:
                stage.setCh(x,y,' ')
                stage.setCh(x-1,y,'B')


class Life(qweShoot):
    FRICTION_AIR=(10.0,10.0)
    BOUNCE=-.8
    ShowLife=MaxLife=Life=10
    LifeCollisionCost=0
    LifeCollisionSpriteCost=-10

    GRAVITY=1
    SOLID=False
    FSOLID=False

    RADIUS=0

    MARGIN_LEFT=+4
    MARGIN_RIGHT=-4
    MARGIN_TOP=+4
    MARGIN_BOTTOM=-4

    def __init__(self, stage, pos,imgfile, life):

        qweSprite.__init__(self,stage)

        self.image1 = pygame.image.load(imgfile)
        self.image = self.image1.copy()
        self.rect = self.image.get_rect()
        self.ShowLife=self.MaxLife=self.Life=life

        self.rect.topleft = pos
        self.dx=0
        self.dy=-20
        self.x,self.y,self.w,self.h=self.rect
        self.tickms(5,stage);

    def tickms(self, ms, stage):
        if self.RADIUS<16:
            self.RADIUS+=ms/5000.0

        v=qweSprite.tickms(self, ms, stage)
        if self.psx<0: self.move(8,0)
        if self.psx>=20: self.move(-8,0)

        if self.psy<0: self.move(0,8)
        if self.psy>=15: self.move(0,-8)
        return v