#   encoding: UTF-8
import pygame
import re
import qwesprites

class Stage_Texture:
    char=''
    filename=''
    texture=None
    def __repr__(self):
        return "(char:%s, filename:%s, texture:%s)" % (self.char, self.filename,repr(self.texture))

class Stage:
    BlockSize=(32,32)
    StageData=[""]
    StageTitle=""
    Texture={}
    Scroll=[0,0]
    simbolos={}
    listasimbolos="!·$%&/()=|@#~"
    Surface=None
    background=None
    Tajundra=None
    Tajundra_life=120
    sprites=[]
    background_sprites=[]
    NextStage=""
    frame=0
    rect_changed=[]
    check_stage_ms=0

    def __init__(self,fichero):
        self.BlockSize=(32,32)
        self.StageData=[""]
        self.StageTitle=""
        self.Texture={}
        self.Scroll=[0,0]
        self.simbolos={}
        self.listasimbolos="!·$%&/()=|@#~"
        self.Surface=None
        self.background=None
        self.Tajundra=None
        self.Tajundra_life=120
        self.sprites=[]
        self.background_sprites=[]
        self.frame=0
        self.rect_changed=[]
        self.load(fichero)

    def getCh(self,x,y):
        if (x<0): return 't'
        if len(self.StageData)<=y: return 't'
        if y>=0 and len(self.StageData[y])<=x: return 't'
        if (y<0): return ' '
        return self.StageData[y][x]
    def setCh(self,x,y,ch):
        if (x<0): return
        if len(self.StageData)<=y: return
        if (y<0): return
        if len(self.StageData[y])<=x: return
        if self.StageData[y][x]==ch: return
        self.StageData[y][x]=ch
        rect=pygame.Rect((
            x*self.BlockSize[0],y*self.BlockSize[1],
            self.BlockSize[0],self.BlockSize[1]))
        self.rect_changed+=[rect]


    def checkPortal(self):
        x=0
        y=0
        pch='P'
        for line in self.StageData:
            x=0
            for ch in line:
                if (ch=='p' or ch=='P'):
                    px=x
                    py=y


                if (ch=='Z'):
                    if (self.StageData[y-1][x]!='z'):
                        pch='p'

                x+=1
            y+=1
        self.setCh(px,py,pch)

    def checkMovingBlocks(self):
        x=0
        y=0
        blocks=[]
        for line in self.StageData:
            x=0
            for ch in line:
                if (ch=='B'):
                    blocks+=[(x,y)]

                x+=1
            y+=1
        for x,y in blocks:
            if self.getCh(x,y+1)==' ':
                testrect=pygame.Rect((x)*32,(y+1)*32,32,32)
                put_in_place=True
                for sprite in self.sprites:
                    if sprite.FSOLID and sprite.rect.colliderect(testrect):
                        put_in_place=False
                        sprite.ShowLife=sprite.Life
                        sprite.Life-=40

                if put_in_place:
                    self.setCh(x,y,' ')
                    self.setCh(x,y+1,'B')

    def do_stageChecks(self):
        self.checkPortal()
        self.checkMovingBlocks()


    def fulldraw(self,screen,msec):
        self.frame+=1
        self.check_stage_ms+=msec
        if self.check_stage_ms>200:
            self.check_stage_ms=0
            self.do_stageChecks()
        for rect in self.rect_changed:
            self.draw(screen,rect,True)

        v_rect=self.rect_changed
        self.rect_changed=[]


        for sprite in self.sprites:
            v_rect+=sprite.tickms(msec,self)

        collision=False
        while collision:
            collision=False
            delete_list=set([])
            for index,rect in enumerate(v_rect):
                if index in delete_list:
                    continue
                l=rect.collidelistall(v_rect)
                del l[0]
                l=set(l)
                l-=delete_list
                if len(l)>0:
                    t=[]
                    collision=True
                    for lindex in l:
                        t+=[v_rect[lindex]]

                    delete_list|=l
                    rect.unionall_ip(t)

            delete_list=list(delete_list)
            delete_list.sort()

            for index in reversed(delete_list):
                if index<len(v_rect):
                    del v_rect[index]

        for rect in v_rect:
            self.draw(screen,rect)

        keys=[]
        lensp=len(self.sprites)-1
        for key,sprite in enumerate(reversed(self.sprites)):
            if sprite.alive:
                sprite.draw(screen,v_rect)
            else:
                del self.sprites[lensp-key]

        for rect in v_rect:
            pygame.display.update(rect)




    def draw(self,screen,rect=None,redraw=False):

        if not self.Surface:
            self.Surface=screen.copy()
            redraw=True
            self.rect_changed=[]

        if not rect:
            rect=self.Surface.get_rect()

        if redraw:
            if not self.background:
                self.background = pygame.image.load("fondo1.png")
            #self.Surface.fill((60,16,16))
            #self.Surface.blit(self.background,(0,0))

            rect_seq=[rect]
            for sprite in self.background_sprites:
                rect_seq+=sprite.tickms(10,self)

            for rect_1 in rect_seq:
                self.Surface.fill((60,16,16),rect_1)
                self.Surface.blit(self.background,rect_1,rect_1)


            x=0
            y=0
            for line in self.StageData:
                x=0
                for ch in line:
                    rect_1=pygame.Rect(self.Scroll[0]+x*self.BlockSize[0],
                            self.Scroll[1]+y*self.BlockSize[1]
                        ,self.BlockSize[0],self.BlockSize[1])
                    if rect_1.colliderect(rect):
                        if ((ord(ch)>=ord('A') and ord(ch)<=ord('Z')) or
                            (ord(ch)>=ord('a') and ord(ch)<=ord('z'))):
                            tex=self.Texture[ch].texture
                            if tex:
                                self.Surface.blit(tex,rect_1)
                        else:
                            self.Surface.fill((60,16,16),rect_1)
                            self.Surface.blit(self.background,rect_1,rect_1)

                        #screen.blit(self.Surface,rect_1,rect_1)
                    x+=1
                y+=1

            for sprite in self.background_sprites:
                sprite.draw(self.Surface,[rect])

            screen.blit(self.Surface,rect,rect)

        else:
            screen.blit(self.Surface,rect,rect)


    def load(self,fichero):
        print "Cargando pantalla %s ...." % fichero
        self.NextStage=fichero

        comando=""
        argumentos=[]
        datos=[]
        for s in self.listasimbolos:
            self.simbolos[s]=[]

        whitespace=Stage_Texture();
        whitespace.char=' '
        self.Texture[' ']=whitespace

        f1=open(fichero,"r")
        nlinea=0
        for line in f1:
            nlinea+=1
            line=line[:-1]
            ex=re.search("%([\w]{4,64}) ([\S ]*)",line)

            if len(line)>3:
                if ex:
                    if comando:
                        self.exec_stcommand(comando,argumentos,datos)
                    argumentos=[]
                    datos=[]
                    ex1=ex.groups()
                    comando=ex1[0]
                    if len(ex1)>1:
                        argumentos=ex1[1].split(" ")

                elif line[0]=='[' and line[-1]==']':
                    datos+=[list(line[1:-1])]
                else:
                    print "Error: Linea %d no se reconoce la secuencia '%s'" % (nlinea,line)

        if comando:
            self.exec_stcommand(comando,argumentos,datos)

        ################# VER POSICION SPRITES ##########
        x=y=0
        for line in self.StageData:
            x=0
            for ch in line:
                if ((ord(ch)>=ord('A') and ord(ch)<=ord('Z')) or
                    (ord(ch)>=ord('a') and ord(ch)<=ord('z'))):
                    pass
                elif ord(ch)>=ord('0') and ord(ch)<=ord('9'):
                    pass
                elif ch in self.listasimbolos:
                    self.simbolos[ch] += [(x*self.BlockSize[0],y*self.BlockSize[1])]
                    self.StageData[y][x]=' '
                x+=1
            y+=1

        qwesprites.qweTextBasic(self,2,2,self.StageTitle)
        self.Tajundra=qwesprites.Tajundra(self)
        self.Tajundra.MaxLife=self.Tajundra.Life=self.Tajundra.ShowLife=self.Tajundra_life

        for c in self.Texture:
            tex=self.Texture[c]
            print tex
            if tex.filename:
                tex.texture = pygame.image.load(tex.filename)

        self.checkPortal()

    def exec_stcommand(self,cmd,arg,data):
        if cmd=='BlockSize':
            self.BlockSize=(int(arg[0]),int(arg[1]))
        elif cmd=='Texture':
            tex=Stage_Texture();
            tex.char=arg[0]
            tex.filename=arg[1]
            self.Texture[arg[0]]=tex

        elif cmd=='StageTitle':
            self.StageTitle=" ".join(arg)
        elif cmd=='StageData':
            if len(data)!=int(arg[1]):
                print "Error: No coincide el tamaño de datos Y (%d -> %d) en StageData" % (len(data),int(arg[1]))
            else:
                for line in data:
                    if len(line)!=int(arg[0]):
                        print "Error: No coincide el tamaño de datos X (%d -> %d) en StageData" % (len(line),int(arg[0]))
                        data=[""]
                        break
                self.StageData=data
        elif cmd=='PlaceText':
            tx,ty=self.BlockSize
            x=int(arg[0])*tx+2
            y=int(arg[1])*ty+2
            for line in data:
                qwesprites.qweTextBasic(self,x,y,"".join(line))
                y+=10
        elif cmd=='CreateTajundraWithLife':
            self.Tajundra_life=int(arg[0])
        elif cmd=='NextStage':
            self.NextStage=arg[0]

        else:
            print "Error: Comando '%s' desconocido." % cmd
