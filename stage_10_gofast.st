%BlockSize 32 32
%Texture N dragonN1.png
%Texture R dragonR1.png
%Texture V dragonV2.png

%Texture M pedrusco_cal1.png
%Texture N pedrusco_cal2.png
%Texture m pedrusco_cal3.png
%Texture n pedrusco_cal4.png

%Texture b pedrusco1.png
%Texture B pedrusco1M.png

%Texture t tierra1.png
%Texture r tierra2.png
%Texture p portal.png
%Texture P portal_abierto.png
%Texture Z piedra_runaz.png
%Texture z runaz.png

%CreateTajundraWithLife 120

%StageTitle Episode 1 - Level 1 - Go Faster

%StageData 20 15
[                    ]
[@      $     $      ]
[bbMMbMMbMMbMMbMMb   ]
[                b   ]
[                b   ]
[                b   ]
[$ | $ | $ | $ |     ]
[bbbbbbbbbbbbbbbbb   ]
[                   b]
[    z    p    $   bb]
[b  bbb  bbb  bbbbbbb]
[bb                  ]
[bbb                 ]
[bbbb  |  $ |   $   Z]
[bbbbbtrtrtrtrtrtrttr]


%NextStage stage_11.st


