%BlockSize 32 32
%Texture N dragonN1.png
%Texture R dragonR1.png
%Texture V dragonV2.png

%Texture M pedrusco_cal1.png
%Texture N pedrusco_cal2.png
%Texture m pedrusco_cal3.png
%Texture n pedrusco_cal4.png

%Texture b pedrusco1.png
%Texture B pedrusco1M.png

%Texture t tierra1.png
%Texture r tierra2.png
%Texture p portal.png
%Texture P portal_abierto.png
%Texture Z piedra_runaz.png
%Texture z runaz.png

%CreateTajundraWithLife 240

%StageTitle Tutorial 3 - Moving blocks

%StageData 20 15
[                    ]
[ @           B      ]
[trtrtrtrtrtrtzt     ]
[bbbb         bb     ]
[                    ]
[    trtrtr     |  | ]
[   bb    brtrtrtrtrt]
[   bb               ]
[   bb         $   z ]
[Z   B  M     trtrtrt]
[rtrtrtrtrt          ]
[        bbZ    $    ]
[         bbbbbbbbb  ]
[    p               ]
[    trtrtrtrtrtrtrtr]

%PlaceText 7 0
[Some blocks are movable]
[shoot'em and they will move.]

%PlaceText 15 2
[You can use UP and DOWN]
[arrows with SPACE to ]
[get better aim]

%PlaceText 6 8
[Destroy the white block and ]
[use the moving block to]
[help you reaching the rune]

%NextStage stage_04_largeshoot.st



